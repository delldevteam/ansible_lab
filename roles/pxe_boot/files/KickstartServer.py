#!/usr/bin/python2
import time
import BaseHTTPServer

import sqlite3 as sql
import argparse

HOST_NAME = 'flex-mgt-data' # !!!REMEMBER TO CHANGE THIS!!!
HTTP_PORT = 9440 # Maybe set this to 9000.
HTTP_SERVER = '109.0.1.250'
DATABASE = 'passed in as argument'

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        
    def do_GET(s):
        print(s.path)
        """Respond to a GET request."""
        if s.path == '/ks':
            s.send_response(200)
            s.send_header("Content-type", "text/plain")
            s.end_headers()
            conn = sql.connect(DATABASE)
            cursor = conn.cursor()
            cursor.execute("select os_ip,os_hostname,os_deploy_mac,os_netmask FROM nodes WHERE os_ip = \'{}\';".format(s.client_address[0]))
            rows =list( cursor.fetchall())
            cursor.close()
            conn.close()
            (os_ip, os_hostname, os_deploy_mac, os_netmask) = rows[0]
            print("sending ks response to {0} mac {1}".format(os_ip, os_deploy_mac))
            with open("/root/ks.cfg") as ks_file:
                for line in ks_file:
                    s.wfile.write(line.format(ip=os_ip,
                                              mac=os_deploy_mac,
                                              gateway="",
                                              netmask=os_netmask,
                                              hostname=os_hostname,
                                              http_server=HTTP_SERVER+':'+str(HTTP_PORT)
                    ))
        elif s.path == '/installed':
            s.send_response(200)
            s.send_header("Content-type", "text/plain")
            s.end_headers()
            print("sending installed response to {0}".format(s.client_address[0]))
            conn = sql.connect(DATABASE)
            cursor = conn.cursor()            
            cursor.execute("UPDATE nodes SET os_state = 'installed' WHERE os_ip = \'{}\';".format(s.client_address[0]))
            conn.commit()
            cursor.close()
            conn.close()
            s.wfile.write("Installed\n")
        else:
            s.send_response(400)
            s.send_header("Content-type", "text/html")
            s.end_headers()
            s.wfile.write("<html><head><title>Invalid url</title></head>")
            s.wfile.write("<body><p>fail</p>")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--database', type=str, help='database filename.')
    args, unknown = parser.parse_known_args()
    DATABASE = args.database
    print("Database used: {0}".format(DATABASE))
    server_class = BaseHTTPServer.HTTPServer
    httpd = server_class((HOST_NAME, HTTP_PORT), MyHandler)
    print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, HTTP_PORT)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, HTTP_PORT)
