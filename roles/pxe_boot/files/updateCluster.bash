#!/bin/bash
cd /home/vxmanager/vxmanager/ansible-flex
export ANSIBLE_HOME=$( git rev-parse --show-toplevel)/ansible-flex;
envsubst < ${ANSIBLE_HOME}/ansible.cfg > ~/.ansible.tmp.cfg ;
export ANSIBLE_CONFIG=~/.ansible.tmp.cfg;
while :
do
    ansible-playbook updateSystemFiles.yml
    ansible-playbook pxe-boot-once.yml
    sleep 60
done
