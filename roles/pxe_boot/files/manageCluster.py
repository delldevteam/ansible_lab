#!/usr/bin/python

import sys
from shutil import copyfile
import argparse

import sqlite3 as sql

import logging
from logging.handlers import RotatingFileHandler

from python_hosts import Hosts, HostsEntry

class BuildDhcpStatics(object):
    def __init__(self, database):
        super(BuildDhcpStatics, self).__init__()
        self.conn = sql.connect(database)
        hosts = Hosts('/etc/hosts')
        entries = [entry for entry in hosts.entries if 'tftp-server' in entry.names]
        if len(entries) == 0:
            logging.critical("Unable to find tftp-server entry in /etc/hosts")
        # next statment will intentionally fail if tftp-server not in /etc/hosts
        self.tftpServer = entries[0].address

        
    def run(self):
        cursor = self.conn.cursor()
        cursor.execute("select os_ip,os_hostname,os_deploy_mac, os_type FROM nodes WHERE os_hostname <> '';")
        rows =list( cursor.fetchall())
        with open('vxmanager.static.addresses', 'w') as file:
            for (os_ip, os_hostname, os_deploy_mac, os_type) in rows:
                file.write("host {} {{\n".format(os_hostname+'-esxi'))
                file.write("  hardware ethernet {};\n".format(os_deploy_mac))
                file.write("  fixed-address {};\n".format(os_ip))
                file.write("  # specifies the tftp server address\n")
                file.write("  next-server {};\n".format(self.tftpServer))
                file.write("  option pxelinux.configfile \"pxelinux.cfg/{}\";\n".format(os_type))
                file.write("}\n")
        copyfile('vxmanager.static.addresses', '/etc/dhcp/vxmanager.static.addresses')


class BuildHostsEntries(object):
    def __init__(self, database):
        super(BuildHostsEntries, self).__init__()
        self.conn = sql.connect(database)
        hosts = Hosts('/etc/hosts')
        entries = [entry for entry in hosts.entries if 'tftp-server' in entry.names]
        if len(entries) == 0:
            logging.critical("Unable to find tftp-server entry in /etc/hosts")
        # next statment will intentionally fail if tftp-server not in /etc/hosts
        self.tftpServer = entries[0].address
        
    def run(self):
        hosts = Hosts('./hosts.generated')
        newEntries=[]
        hosts.import_file('/etc/hosts')
        cursor = self.conn.cursor()
        # fix idrac entries
        cursor.execute("select drac_ip_fqdn,os_hostname FROM nodes WHERE os_hostname <> '';")
        rows =list( cursor.fetchall())
        for (drac_ip, os_hostname) in rows:
            hosts.remove_all_matching('address={}'.format(drac_ip))
            newEntries.append(HostsEntry(entry_type='ipv4', address=drac_ip, names=[os_hostname+'-idrac']))

        # fix esxi entries
        cursor.execute("select os_ip,os_hostname FROM nodes WHERE os_hostname <> '';")
        rows =list( cursor.fetchall())
        for (os_ip, os_hostname) in rows:
            hosts.remove_all_matching('address={}'.format(os_ip))
            newEntries.append(HostsEntry(entry_type='ipv4', address=os_ip, names=[os_hostname+'-esxi']))
        #
        counts = hosts.add(entries=newEntries, force=True)
        #print("{0} {1} {2} {3} {4}".format(counts['ipv4_count'], counts['ipv6_count'], counts['invalid_count'],
        #                                   counts['duplicate_count'], counts['replaced_count']))
        hosts.write()
        copyfile('./hosts.generated', '/etc/hosts')


if __name__ == "__main__":
    logPath="./"
    log_formatter = logging.Formatter('%(asctime)s %(levelname)s,%(message)s',
                                      datefmt='%b %d,%Y %H:%M:%S')
    my_handler = RotatingFileHandler(filename=logPath + '/manageCluster.log',
                                     mode='a',
                                     maxBytes=1*1024*1024,  # 1 MB
                                     backupCount=10,
                                     encoding=None,
                                     delay=0)
    app_log = logging.getLogger()    
    my_handler.setLevel(logging.INFO)
    my_handler.setFormatter(log_formatter)
    app_log.setLevel(logging.INFO)
    app_log.addHandler(my_handler)

    logging.info("ManageCluster.py started")

    parser = argparse.ArgumentParser()
    parser.add_argument('--database', type=str, help='database filename.')
    args, unknown = parser.parse_known_args()
    
    dhcpStatics = BuildDhcpStatics(args.database)
    dhcpStatics.run()
    hostsEntries = BuildHostsEntries(args.database)
    hostsEntries.run()
