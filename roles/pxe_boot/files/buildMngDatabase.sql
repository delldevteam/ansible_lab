DROP TABLE IF EXISTS nodes;
CREATE TABLE nodes (
       os_state text,
       os_ip text,
       os_netmask text,
       os_gateway text,
       os_pridns text,
       os_secdns text,
       os_vlan text,
       os_deploy_mac text,
       os_hostname text,
       os_domain text,
       os_username text,
       os_password text,
       os_type text,
       pxe_boot_device text,
       drac_ip_fqdn text NOT NULL PRIMARY KEY,
       drac_username text,
       drac_password text);
       




